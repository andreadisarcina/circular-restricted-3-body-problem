import numpy as np
import matplotlib.pyplot as plt

#mass ratio
mu=0.012277471
#tolerance for adaptive time step
tol1=1e-3
#tolerance for final results precision
tol=1e-10
#initial dynamical state
x0=np.array([0.8,0.,0.,0.35])

#runge kutta 4 integrator over a time step
def rungekutta4(funz,yn,hn):
	k1=hn*funz(yn)
	k2=hn*funz(yn+0.5*k1)
	k3=hn*funz(yn+0.5*k2)
	k4=hn*funz(yn+k3)
	ynew=yn+1./6.*(k1+2*k2+2*k3+k4)
	return ynew

#runge kutta 4 integrator over a time step for phi matrix
def rungekutta4phi(funz,xn,phin,hn):
	k1=hn*funz(xn,phin)
	k2=hn*funz(rungekutta4(f,xn,0.5*hn),phin+0.5*k1)
	k3=hn*funz(rungekutta4(f,xn,0.5*hn),phin+0.5*k2)
	k4=hn*funz(rungekutta4(f,xn,hn),phin+k3)
	phinew=phin+1./6.*(k1+2*k2+2*k3+k4)
	return phinew

#function x'=f(x) for the differential equations of dynamical state (x'=vx, y'=vy, vx'=ax, vy'=ay)
def f(Z):
	r1=np.sqrt((Z[0]+mu)**2.+Z[1]**2.)
	r2=np.sqrt((Z[0]-1.+mu)**2.+Z[1]**2.)
	f0=Z[2]
	f1=Z[3]
	f2=2.*Z[3]+Z[0]-(1.-mu)*(Z[0]+mu)/r1**3.-mu*(Z[0]-1.+mu)/r2**3.
	f3=-2.*Z[2]+Z[1]-(1.-mu)*Z[1]/r1**3.-mu*Z[1]/r2**3.
	return np.array([f0,f1,f2,f3])

#function phi'=f(t,phi)=f(x(t),phi) for the differential equations of phi matrix (phi'=A*phi)
def function(X,Phi):
	#computation of the gradients of the effective potential and of matrix A
	R1=np.sqrt((X[0]+mu)**2.+X[1]**2.)
	R2=np.sqrt((X[0]-1.+mu)**2.+X[1]**2.)
	Uxx=1.-(1.-mu)/R1**3.-mu/R2**3.+3.*(1.-mu)*(X[0]+mu)**2./R1**5.+3.*mu*(X[0]-1.+mu)**2./R2**5.
	Uyy=1.-(1.-mu)/R1**3.-mu/R2**3.+3.*((1.-mu)/R1**5.+mu/R2**5.)*X[1]**2.
	Uxy=3.*((1.-mu)*(X[0]+mu)/R1**5.+mu*(X[0]-1.+mu)/R2**5.)*X[1]
	A=np.array([[0.,0.,1.,0.],
		        [0.,0.,0.,1.],
		        [Uxx,Uxy,0.,2.],
		        [Uxy,Uyy,-2.,0.]])
	return np.dot(A,Phi)

def Jacobi(Y):
	R1=np.sqrt((Y[0]+mu)**2.+Y[1]**2.)
	R2=np.sqrt((Y[0]-1.+mu)**2.+Y[1]**2.)
	return Y[0]**2.+Y[1]**2.-Y[2]**2.-Y[3]**2.+2.*(1.-mu)/R1+2.*mu/R2

#dummy x
x=np.array([1.,1.,1.,1.])
#loop to reach convergence
while(abs(x[2])>1e-5):

	#initial time step
	h=1e-4
	#dynamical state at time t (at the beginning it is x0)
	x=np.copy(x0)
	#initial time instant
	t=0.
	#differential state transition matrix at the initial time instant
	phi=np.array([[1.,0.,0.,0.],
		          [0.,1.,0.,0.],
	    	      [0.,0.,1.,0.],
	        	  [0.,0.,0.,1.]])

	#time loop
	while(True):

		#saving old value of the state
		xold=np.copy(x)
		#integration of dynamical state over a time step
		xnew=rungekutta4(f,x,h)
		#adaptive step size corrections
		if((abs(xnew[1]-x[1])<0.1*tol1*abs(xnew[1])) and (x[1]>0.01) and (h<1e-4)):
			h*=2.
			xnew=rungekutta4(f,x,h)
		else:
			while((abs(xnew[1]-x[1])>tol1*abs(xnew[1])) and (x[3]<0.) and (x[1]<0.01)):
				h/=10.
				xnew=rungekutta4(f,x,h)
		#value of the state after the time step
		x=np.copy(xnew)
		t+=h
	
		#integration of phi over the current time state
		phi=rungekutta4phi(function,xold,phi,h)

		#exit condition for the time loop (y==0 to at least 10 significant digits)
		if((abs(x[1])<tol) and (x[3]*x0[3]<0.)):
			break

	#parameters to choose the strategy to compute dx0 and dvy0 (correction for initial x and correction for initial y')
	p=0    #p=0,1
	q=1    #q=1,2,3,4

	#free final time
	if(p==0):
		#computation of useful coefficients
		r1=np.sqrt((x[0]+mu)**2.+x[1]**2.)
		r2=np.sqrt((x[0]-1.+mu)**2.+x[1]**2.)
		ax=2.*x[3]+x[0]-(1.-mu)*(x[0]+mu)/r1**3.-mu*(x[0]-1.+mu)/r2**3.
		ay=-2.*x[2]+x[1]-(1.-mu)*x[1]/r1**3.-mu*x[1]/r2**3.
		Ax=phi[0,0]*x[3]-phi[1,0]*x[2]
		Bx=phi[0,3]*x[3]-phi[1,3]*x[2]
		Ax1=phi[2,0]*x[3]-phi[1,0]*ax
		Bx1=phi[2,3]*x[3]-phi[1,3]*ax
		Ay1=phi[3,0]*x[3]-phi[1,0]*ay
		By1=phi[3,3]*x[3]-phi[1,3]*ay
		#fixed initial position
		if(q==1):
			dx0=0.
			dvy0=-x[2]*x[3]/Bx1
		#fixed initial velocity
		if(q==2):
			dx0=-x[2]*x[3]/Ax1
			dvy0=0.
		#fixed final position
		if(q==3):
			dx0=Bx/(Ax*Bx1-Ax1*Bx)*x[2]*x[3]
			dvy0=Ax/(Ax1*Bx-Ax*Bx1)*x[2]*x[3]
		#fixed final (transverse) velocity
		if(q==4):
			dx0=By1/(Ay1*Bx1-Ax1*By1)*x[2]*x[3]
			dvy0=Ay1/(Ax1*By1-Ay1*Bx1)*x[2]*x[3]
		#correction on final time
		dt=-(phi[1,0]*dx0+phi[1,3]*dvy0)/x[3]
	#fixed final time
	if(p==1):
		dx0=phi[1,3]/(phi[1,0]*phi[2,3]-phi[2,0]*phi[1,3])*x[2]
		dvy0=phi[1,0]/(phi[2,0]*phi[1,3]-phi[1,0]*phi[2,3])*x[2]
		dt=0.

	#array of corrections
	deltax0=np.array([dx0,0.,0.,dvy0])
	#correct initial state
	x0=x0+deltax0

print(x0[0],x0[1],x0[2],x0[3])

#correction on time
tf=t+dt
#period of the orbit
T=2.*tf
print(T)

#reset initial dynamical state
x=np.copy(x0)
#reset intial time step size and time instant
h=1e-4
t=0.

#Jacobi constant of the periodic orbit
C0=Jacobi(x0)
print(C0)
#list of Jacobi constants
C=[C0]
#list of time instants
time=[t]
#lists of x and y of m3 in synodic frame and of x and y of m1, m2 and m3 in inertial frame (at t=0 the frames coincide)
xm3=[x[0]]
ym3=[x[1]]
xinertialm3=[x[0]]
yinertialm3=[x[1]]
xinertialm1=[-mu]
yinertialm1=[0.]
xinertialm2=[1.-mu]
yinertialm2=[0.]

#loop over a period
while(t<T):

	#integration of correct dynamical state over a time step
	xnew=rungekutta4(f,x,h)
	#adaptive step size corrections
#	if((abs(np.linalg.norm(xnew)-np.linalg.norm(x))<0.001*tol1*np.linalg.norm(xnew))):
#		h*=2.
#		xnew=rungekutta4(f,x,h)
#	else:
#		while((abs(np.linalg.norm(xnew)-np.linalg.norm(x))>0.01*tol1*np.linalg.norm(xnew))):
#			h/=10.
#			xnew=rungekutta4(f,x,h)
	#value of the state after the time step
	x=np.copy(xnew)
	t+=h

	#Jacobi constant at time t
	C.append(Jacobi(x))
	time.append(t)

	#rotation matrix from synodic frame to inertial frame
	R=np.array([[np.cos(-t),np.sin(-t)],
		        [-np.sin(-t),np.cos(-t)]])
	#conversion from synodic to inertial position
	xinertial=np.dot(R,x[0:2])
	#positions of m1 and m2 in the inertial frame
	xm1=np.dot(R,np.array([-mu,0.]))
	xm2=np.dot(R,np.array([1.-mu,0.]))

	xm3.append(x[0])
	ym3.append(x[1])
	xinertialm3.append(xinertial[0])
	yinertialm3.append(xinertial[1])
	xinertialm1.append(xm1[0])
	yinertialm1.append(xm1[1])
	xinertialm2.append(xm2[0])
	yinertialm2.append(xm2[1])

#Hill zero velocity curve
xhill=np.linspace(-2.,2.,1000)
yhill=np.linspace(-2.,2.,1000)
Xhill,Yhill=np.meshgrid(xhill,yhill)
r1hill=np.sqrt((Xhill+mu)**2.+Yhill**2.)
r2hill=np.sqrt((Xhill-1.+mu)**2.+Yhill**2.)
F=Xhill**2.+Yhill**2.+2.*(1.-mu)/r1hill+2.*mu/r2hill-C0

#equilibrium points
L4=np.array([0.5-mu,np.sqrt(3.)/2.])
L5=np.array([0.5-mu,-np.sqrt(3.)/2.])
tolerance=1e-6
fL1=lambda xi: xi**5.+(4.*mu-2.)*xi**4.+(6.*mu**2.-6.*mu+1.)*xi**3.+(4.*mu**3.-6.*mu**2.+4.*mu-1.)*xi**2.+(mu**4.-2.*mu**3.+5.*mu**2.-4.*mu+2.)*xi+(2.*mu**3.-3.*mu**2.+3.*mu-1.)
der1=lambda xi: 5.*xi**4.+(4.*mu-2.)*4.*xi**3.+(6.*mu**2.-6.*mu+1.)*3.*xi**2.+(4.*mu**3.-6.*mu**2.+4.*mu-1.)*2*xi+(mu**4.-2.*mu**3.+5.*mu**2.-4.*mu+2.)
xL1=0.
xL1old=10.
while(abs(xL1-xL1old)>tolerance):
	xL1old=xL1
	xL1=xL1-fL1(xL1)/der1(xL1)
L1=np.array([xL1,0.])
fL2=lambda xi: xi**5.+(4.*mu-2.)*xi**4.+(6.*mu**2.-6.*mu+1.)*xi**3.+(4.*mu**3.-6.*mu**2.+2.*mu-1.)*xi**2.+(mu**4.-2.*mu**3.+mu**2.-4.*mu+2.)*xi+(-3.*mu**2.+3.*mu-1.)
der2=lambda xi: 5.*xi**4.+(4.*mu-2.)*4.*xi**3.+(6.*mu**2.-6.*mu+1.)*3.*xi**2.+(4.*mu**3.-6.*mu**2.+2.*mu-1.)*2*xi+(mu**4.-2.*mu**3.+mu**2.-4.*mu+2.)
xL2=0.
xL2old=10.
while(abs(xL2-xL2old)>tolerance):
	xL2old=xL2
	xL2=xL2-fL2(xL2)/der2(xL2)
L2=np.array([xL2,0.])
fL3=lambda xi: xi**5.+(4.*mu-2.)*xi**4.+(6.*mu**2.-6.*mu+1.)*xi**3.+(4.*mu**3.-6.*mu**2.+2.*mu+1.)*xi**2.+(mu**4.-2.*mu**3.+mu**2.+4.*mu-2.)*xi+(3.*mu**2.-3.*mu+1.)
der3=lambda xi: 5.*xi**4.+(4.*mu-2.)*4.*xi**3.+(6.*mu**2.-6.*mu+1.)*3.*xi**2.+(4.*mu**3.-6.*mu**2.+2.*mu+1.)*2*xi+(mu**4.-2.*mu**3.+mu**2.+4.*mu-2.)
xL3=0.
xL3old=10.
while(abs(xL3-xL3old)>tolerance):
	xL3old=xL3
	xL3=xL3-fL3(xL3)/der3(xL3)
L3=np.array([xL3,0.])

#plot of the orbits in synodic and inertial frames
fig,(ax1,ax2)=plt.subplots(1,2,figsize=plt.figaspect(0.5))
ax1.scatter(-mu,0.,color='g',s=10,label='m1')
ax1.scatter(1.-mu,0.,color='r',s=2,label='m2')
ax1.scatter(xm3,ym3,color='b',s=0.5,label='m3')
ax1.contour(Xhill,Yhill,F,[0],linewidths=0.5)
ax1.scatter(L1[0],L1[1],color='k',s=0.5)
ax1.scatter(L2[0],L2[1],color='k',s=0.5)
ax1.scatter(L3[0],L3[1],color='k',s=0.5)
ax1.scatter(L4[0],L4[1],color='k',s=0.5)
ax1.scatter(L5[0],L5[1],color='k',s=0.5)
ax2.scatter(xinertialm1,yinertialm1,color='g',s=1,label='m1')
ax2.scatter(xinertialm2,yinertialm2,color='r',s=1,label='m2')
ax2.scatter(xinertialm3,yinertialm3,color='b',s=1,label='m3')
ax1.legend()
ax2.legend()
ax1.set_title('Synodic frame')
ax2.set_title('Inertial frame')
ax1.set_xlabel('x')
ax1.set_ylabel('y')
ax2.set_xlabel('x')
ax2.set_ylabel('y')
fig.tight_layout()
fig.show()
#plot of the Jacobi constant over a period
fig1=plt.figure()
plt.plot(time,C,'b-',lw=1)
plt.xlabel('time')
plt.ylabel('Jacobi constant')
fig1.tight_layout()
fig1.show()
plt.show()