# Circular Restricted 3-Body Problem

This script takes guesses for the initial state associated to a symmetric periodic orbit in the Circular Restricted 3-Body Problem, and it computes the exact initial conditions to define periodic orbits close to each initial guess.

The tested initial guesses for the states (x0,y0,vx0,vy0) are (0.3,0.,0.,1.8), (0.5,0.,0.,1.2), (0.7,0.,0.,0.53), (0.8,0.,0.,0.35). The mass ratio between the 2 bodies is the Sun-Jupiter one (0.012277471).

The script uses the method of differential correction and the Mirror Configuration Theorem.

The script gives the final estimate for the initial state, the period of the orbit and the value of the Jacobi constant.

The output plot shows the orbits both in the synodic and in the inertial reference frames.

Also a plot of the Jacobi constant during an orbital period is produced.

The plot of the orbit shows also the Hill zero velocity curve corresponding to its Jacobi constant. The Lagrangian and Eulerian points are marked.
